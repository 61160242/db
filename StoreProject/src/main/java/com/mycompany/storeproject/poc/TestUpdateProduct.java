/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gunm2
 */
public class TestUpdateProduct {
    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/Coffee.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.println("Database connection.");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Error: Database cannot connection ");
        }

        try {
           String sql = "UPDATE product SET name = ?, price = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"Oh Leing 1");
            stmt.setDouble(2,30);
            stmt.setInt(3, 9);
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row );
        } catch (SQLException ex) {
            Logger.getLogger(TestSelecProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println("Error: Connect close database");
        }
    }
}
